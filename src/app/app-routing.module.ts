import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { HomeComponent } from './pages/home/home.component';
import { FooterComponent } from './common/footer/footer.component';
import { HeaderComponent } from './common/header/header.component';
import { LayoutComponent } from './common/layout/layout.component';
import { TermsAndConditionComponent } from './pages/terms-and-condition/terms-and-condition.component';
import { PrivacyPolicyComponent } from './pages/privacy-policy/privacy-policy.component';

const routes: Routes = [
 { path: '', component: LayoutComponent, children: [
  { path: 'home', component: HomeComponent, data: { title: 'Home' } },
  { path: 'footer', component: FooterComponent, data: { title: 'Footer' } },
  { path: 'header', component: HeaderComponent, data: { title: 'Header' } },
  { path: 'terms-and-condition', component: TermsAndConditionComponent, data: { title: 'Terms and Condition' } },
  { path: 'privacy-policy', component: PrivacyPolicyComponent, data: { title: 'Privacy Policy' } },
  ]
 }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
