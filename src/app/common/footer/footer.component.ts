import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormArray, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {
  year:any;
  registerForm:  FormGroup;
  isSubmitted:any = false;
  constructor(private formBuilder:FormBuilder) {
    this.registerForm  =  this.formBuilder.group({
      email:["", Validators.compose([Validators.required, Validators.email])] ,
    },);
   }
   get formControls() { return this.registerForm.controls; }

   get f(){
     return this.registerForm.controls;
   }
  ngOnInit(): void {
    this.year = new Date().getFullYear()
    
  }
  onSubmit(){
    this.isSubmitted = true;
    if(this.registerForm.invalid){
      return;

    }else {
      console.log(this.registerForm.value);
    }
  }

}
